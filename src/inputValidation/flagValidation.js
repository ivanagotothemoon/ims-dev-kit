const validFlags = require('./validFlags_config');
const menus = require('../menus/menus');

function sortInitInput(args) {
  let keys = Object.keys(args).slice(1);
  let len = keys.length;
  switch (true) {
    case len === 0:
    // console.log(args);
      return [
        {
          status: true,
          flag: { valid: true, val: 'menu' },
          flagVal: { valid: true, val: null, type: 'menu' }
        }
      ]

      break;
    case len > 0:
      return keys.map(key => {
        return makeRes(key, args);
      });
      break;
    default:
      return {}
  }
}

function isMenu(str) {
  return menus.hasOwnProperty(str.toLocaleLowerCase());
}

function isMenuItem(key, val) {
  let { menuItems } = menus[key];
  let final = menuItems.some(({ id }) => id === val);
  return final;
};

function isValid(val, location, args) {

  return checkVal(val, location, args);
  function checkVal(value, location, args) {
    switch (location) {
      case 'primary':
        return [isMenu(value)];
      case 'secondary':
        let dupList = [];
        if (Array.isArray(args[value])) {
          let hasDup = args[value].sort().some(item => {
            if (dupList.indexOf(item) === -1) {
              dupList.push(item);
              return false;
            } else {
              return true
            }
          });
          if (hasDup) return [false];
          // console.log(hasDup);
          return args[value].map(item => {
            return isMenu(value) ? isMenuItem(value, item) : false;
          });
        } else {
          return [isMenu(value) ? isMenuItem(value, args[value]) : false];
        }
      default:
    }
  }
}
function makeRes(key, args) {
  let invalid = [];

  let keyLc = key.toLowerCase();
  let status = isValid(keyLc, 'primary', args)[0] ? true : false;

  let flag = status ? { valid: true, val: keyLc } : statusToFalse(key);
  let flagVal;
  if (typeof (args[key]) === 'string') {
    flagVal = isValid(key, 'secondary', args).indexOf(false) === -1 ? { valid: true, val: args[key], type: 'content' } : statusToFalse(args[key]);
  } else {
    flagVal = { valid: true, val: null, type: 'menu' };
  }
  if (args._.length > 0) args._.forEach(val => { makeInvalid(val) });
  return { status, flag, flagVal, invalid };
  function statusToFalse(val) {
    makeInvalid(val);
    return { valid: null, val };
  }

  function makeInvalid(val) {
    status = false;
    invalid.push(val)
  };
}

function cleanData(selectArr) {
  // console.log(selectArr);throw new Error;
  let invalidItem = false;
  const invalidList = [];
  const contentType = []; 
  selectArr.forEach(item => {
    if (!item.status) {
      invalidList.push(...item.invalid);
      invalidItem = true;
    } else {
      contentType.push(item.flagVal.type);
    }
  });
  if (invalidItem) {
    handleErrorMsg(invalidList, contentType);
  } else if (contentType.indexOf('content') === -1) {
    return menus[selectArr[0].flag.val];
  } else if (contentType.indexOf('content') !== -1) {
    const contentArr = []
    selectArr.forEach(item => {
      let menuItems = menus[item.flag.val].menuItems;
      menuItems.forEach(menuItem => {
        if (menuItem.id === item.flagVal.val) contentArr.push(menuItem);
      })
    })
    return {
      toPrint: contentArr
    }
  }
  // buildMenu();
}

function handleErrorMsg(invalidArr, contentArr) {
  // console.log('IIIIIIIIIIIIIIIIIIIIIIIIII');
  // console.log(invalidArr);
}

module.exports = function init(args) {
  let sortedInputs = sortInitInput(args);
  let send = cleanData(sortedInputs);
  // console.log(send, 'fV 132 ============');
  return send;
}