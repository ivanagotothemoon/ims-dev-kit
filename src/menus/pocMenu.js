module.exports = {
  heading: '👻  🤖  🕶  -POC- 🕶  🤖  👻',
  options: { width: 29, x: 4, y: 2, bg: 'green' },
  menuItems: [
    {
      title: 'INFO',
      id: 'info',
      content: {
        title: 'poc information',
        text: [
          { value: 'poc stands for PROOF OF CONCEPT' }
        ]
      }
    },
    {
      title: 'COMMON COMMANDS',
      id: 'cmd',
      content: {
        title: '✨ ✨ poc common commands✨ ✨',
        text: [
          {
            header: 'tail',
            description: 'show any changes to contents of file',
            value: 'tail -f <location>'
          },
          {
            header: 'apache : alias',
            description: 'start apache | restart apache',
            value: 'myapache start\nmyapache restart'
          },
          {
            header: 'Switch environment : alias',
            description: 'cd to ev config eg cd ~/dev/ev/config, environments: walmart-uat || walmart-dev || tesco',
            value: './switchenv <environment>'
          }
        ]
      }
    },
    {
      title: 'EXIT',
      id: 'exit'
    },
  ],
  menuId: 'poc'
}
