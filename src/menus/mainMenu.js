module.exports = {
  heading: '🦄 🛠 💻 🚀 -IMS Dev Kit- 🚀 💻 🛠 🦄',
  options: { width: 29, x: 4, y: 2 },
  menuItems: [
    { title: 'POC', id: 'poc' },
    { title: 'PM2', id: 'pm2' },
    { title: 'SENCHA', id: 'sencha' },
    { title: 'EXIT', id: 'exit' },
  ],
  menuId: 'main',
}