module.exports = {
  heading: '🔭  🖨  💿  -PM2- 💿  🖨  🔭',
  options: { width: 29, x: 4, y: 2, bg: 'magenta' },
  menuItems: [
    { 
      title: 'INFO', 
      id: 'info', 
      content : {
        title:'pm2 information', 
        text:[
          {value:'PM2 will keep your application forever alive, auto-restarting across crashes and machine restarts'},
          {value:'All your applications are run in the background and can be easily managed'},
        ]
      }
    },
    { 
      title: 'COMMON COMMANDS', 
      id: 'cmd', 
      content : {
        title:'pm2 common commands',
        text:[
          {
            header: 'start & name ',
            value:'pm2 start app.js --name my-api',
            description: 'Start and name a process'
          },
          {
            header: 'stop || restart || delete',
            value:'pm2 stop 0\npm2 restart 0\npm2 delete 0',
            description: 'Stop/Restart/Delete specific process id',
          },
          {
            header: 'stop all || restart all || delete all ',
            value:'pm2 stop all\npm2 restart all\ndelete all',
            description: 'Stop/Restart/Delete all pm2 processes'
          }
        ]
      }
  },
    { title: 'EXIT', id: 'exit' },
  ],
  menuId: 'pm2'
}
