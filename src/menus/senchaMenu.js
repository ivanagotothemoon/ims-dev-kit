module.exports = {
  heading: '-SENCHA-',
  options: { width: 29, x: 4, y: 2, bg: 'green' },
  menuItems: [
    {
      title: 'INFO',
      id: 'info',
      content: {
        title: 'sencha information', 
        text: [
          {value: 'Sencha exists to annoy everyone'}
        ]
      }
    },
    {
      title: 'COMMON COMMANDS',
      id: 'cmd',
      content: {
        title: 'sencha common commands',
        text: [
          {
            header: 'refresh',
            value:'sencha app refresh',
            description: 'run in order for new files added to be included in build'
          },
          {
            header: 'watch',
            value:'sencha app watch',
            description: 'run app and watch for any changes'
          }
        ]
      }
    },
    {
      title: 'EXIT',
      id: 'exit'
    },
  ],
  menuId: 'sencha'
}
