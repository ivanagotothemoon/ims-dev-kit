module.exports = {
  menu : require('./mainMenu'),
  poc : require('./pocMenu'),
  pm2 : require('./pm2Menu'),
  sencha : require('./senchaMenu'),
}