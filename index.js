const Menu = require('terminal-menu');
const menus = require('./src/menus/menus');
const validateFlags = require('./src/inputValidation/flagValidation');

const init = (args)=>output(validateFlags(args));
exports.init = init;


function buildMenu(mData) {
  const cleanTitle = title => title.replace(/\s/g, '');
  let menu = Menu(mData.options);
  menu.reset();
  menu.write(`${mData.heading}\n`);
  menu.write('-------------------------\n');
  const menuObj = {};
  mData.menuItems.forEach(item => {
    menuObj[cleanTitle(item.title)] = item;
    menu.add(item.title);
  });

  menu.on('select', (label) => {
    menu.close();
    if (label === 'EXIT') {
      process.exit();
    } else {
      sortInput(cleanTitle(label), menuObj, mData.menuId);
    }
  });
  process.stdin.pipe(menu.createStream()).pipe(process.stdout);

  process.stdin.setRawMode(true);
  menu.on('close', () => {
    process.stdin.setRawMode(true);
    process.stdin.end();
  });
}
function sortInput(label, obj, menuId) {
  let selectedId = obj[label].id
  if (menus.hasOwnProperty(obj[label].id)) {
    output(menus[selectedId]);
  } else {
    let content = [];
    let items = menus[menuId].menuItems
    items.some(item => {
      if (item.id === selectedId) {
        content.push(item);
        return true;
      } else {
        return false;
      }
    })
    output({ toPrint: content });
  }
}
function output(obj, init) {
  switch (true) {
    case obj.hasOwnProperty('toPrint'):
      printContent(obj.toPrint);
      break;
    case obj.hasOwnProperty('menuId'):
      buildMenu(obj);
      break;
    case obj.hasOwnProperty('errorId'):
      break;
    default:
      break;
  }
}
function printContent(content) {
  content.forEach(item => {
      console.log('\n');
    console.log(item.content.title.toUpperCase());
      console.log('\n');
    item.content.text.forEach((text, index) => {
      let zero = index < 9? '0': '';
      let title = text.hasOwnProperty('header') ? text.header : '';
      console.log(`\u001b[33m${zero+(1+index)}  ${title} - - -\u001b[0m`);
      console.log(`${text.value}`);
      if (text.hasOwnProperty('description')) console.log(`\u001b[32mDESC:\u001b[35m ${text.description}\u001b[0m`)
      console.log('\n');
    })
  })
  process.exit();
}