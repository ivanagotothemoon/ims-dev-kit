#!/usr/bin/env node

const args = require('minimist')(process.argv.slice(2));
var app = require('../index');

// Displays the text in the console
app.init(args);
