# 🚀 IMS devkit 🚀

Small CLI menu listing common info, commands & setup checklists used in the dev team

## Installation

1. clone the repo 
```bash
git clone ims-dev-kit
```

2. npm install globally using -g flag 

```bash
npm install -g ./ims-dev-kit
```
## Uninstall
* To uninstall it (even if installed from a local source) execute 
```bash
npm uninstall -g ims-dev-kit
```

## Usage
#### Devkit command to access **main-menu**
```bash
devkit
```
![Alt Text](https://bitbucket.org/ivanm86/ims-dev-kit/raw/030bbec71f6eee54756d9aefb3a1123c7b5c5685/readmegifs/devkit.gif)
___

#### Access **sub-menu** options directly - **flags**:
```bash
devkit --poc || --pm2 || --sencha
# All sub-menu flags must be prepended with --
```
![Alt Text](https://bitbucket.org/ivanm86/ims-dev-kit/raw/030bbec71f6eee54756d9aefb3a1123c7b5c5685/readmegifs/devkit--poc.gif)
___

#### Access **sub-menu** content directly - **flags**:
```bash
devkit --poc info || cmd
# All sub-menu content flags must be a single instance
```
![Alt Text](https://bitbucket.org/ivanm86/ims-dev-kit/raw/030bbec71f6eee54756d9aefb3a1123c7b5c5685/readmegifs/devkit--poc_cmd.gif)
___

#### Concatenate content from different sub menus :
```bash
devkit --poc cmd --pm2 cmd
# list all poc and pm2 common commands

devkit --sencha info --poc cmd --pm2 info
# list sencha info, poc command and pm2 info
```
![Alt Text](https://bitbucket.org/ivanm86/ims-dev-kit/raw/030bbec71f6eee54756d9aefb3a1123c7b5c5685/readmegifs/devkit--poc_info--sencha_info--pm2_info.gif)
___
## License
[MIT](https://choosealicense.com/licenses/mit/)